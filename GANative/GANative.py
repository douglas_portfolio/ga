import time, array, random, copy, math
import numpy as np
import pandas as ps
import matplotlib.pyplot as plt
from deap import algorithms, base, benchmarks, tools, creator

from FitnessFunctions.fitness_methods import fitness
import Model.IndividualConditions as IndividualConditions

random.seed(a=42)
M = 50

creator.create("FitnessMin", base.Fitness, weights=(-1.0,-1.0))
creator.create("Individual", array.array, typecode='d',
               fitness=creator.FitnessMin)
def dent(individual):
    [individual, y, surface, score] = fitness(individual, M)
    return y, surface, score

toolbox = base.Toolbox()

cond = IndividualConditions.IndividualConditions()
BOUND_LOW, BOUND_UP = cond.minWn, cond.maxWp
NDIM = 2

toolbox.register("evaluate", dent)

def uniform(low, up, size=None):
    try:
        return [random.uniform(a,b) for a, b in zip(low,up)]
    except TypeError:
        return [random.uniform(a,b) for a, b in zip([low]*size, [up]*size)]

IND_SIZE = 4
toolbox.register("attr_float", uniform, BOUND_LOW, BOUND_UP)
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.attr_float)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)
num_samples = 50
limits = [np.arange(BOUND_LOW, BOUND_UP, (BOUND_UP - BOUND_LOW)/num_samples)] * NDIM
sample_x = np.meshgrid(*limits)
flat = []
for i in range(len(sample_x)):
    x_i = sample_x[i]
    flat.append(x_i.reshape(num_samples**NDIM))
example_pop = toolbox.population(n=num_samples**NDIM)

