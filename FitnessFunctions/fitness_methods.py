from FitnessFunctions.units import *
####
# Returns a list composed by the sum of transientPerformanceCondition with timeEqualityPerformance, the surface
# considering the trails and the score of the individual.
#
# @individual: the individual of population
# @M: number of entries on NAND gate
###
def fitness(individual,M):
    wn = individual.wn
    wp = individual.wp
    ln = individual.ln
    lp = individual.lp

    #Physical Conditions
    Cox = 6 * fanton
    unCox = 115 * micro
    upCox = 30 * micro
    Vth_n = 0.4
    Vth_p = - Vth_n
    Vdd = 2.5
    Cj = 0.2 * fanton
    Cjsw = 0.5 * fanton
    # Technology
    LdiffMargin = 0.2
    Ldiff = LdiffMargin * (ln+lp)

    # Expression Load Capacitance

    C = Cox*M*(wp*lp+wn*ln)  + Cj * Ldiff * (wn+wp) + Cjsw * (wn+wp+4*Ldiff)

    tphl = C * Vth_n / ((1 / 2) * unCox * (wn / ln) * (Vdd - Vth_n) ** 2)
    tplh = C * Vth_p / ((1 / 2) * upCox * (wp / lp) * (Vdd - Vth_p) ** 2)

    transientPerformanceCondition = abs((wn / ln) - (M / 2) * (wp / lp))
    timeEqualityPerformance = abs(tphl - tplh)

    y = 1/abs(transientPerformanceCondition + timeEqualityPerformance)

    surface = 1/((wn * ln + wp * lp)*(1.2)*(10**(12))) # Adds 20% for the trails

    score = y + surface

    return [individual, y, surface, score]