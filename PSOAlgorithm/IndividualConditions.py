class IndividualConditions:
    maxWn = 36*micro
    maxLn = 24*micro
    minWn = 32*nano
    minLn = 11*nano
    maxWp = 2*maxWn
    maxLp = 2*maxLn
    minWp = 2*minWn
    minLp = 2*minLn