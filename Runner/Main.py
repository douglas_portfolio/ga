from random import randint, uniform
from datetime import datetime, timedelta
import sys

import matplotlib.pyplot as plt

from GeneticAlgorithm.GeneticAlgorithm import GeneticAlgorithm
from GeneticAlgorithm.GenerationArrays import GenerationArrays

from Model.Population import Population
from Model.IndividualConditions import IndividualConditions
from FitnessFunctions.fitness_methods import fitness
from Model.Individual import Individual

if __name__ == '__main__':

    ga = GeneticAlgorithm()

    # Ploting

    curves_map = {}
    score_curve = []
    best_score_curve = []

    # Time condition
    finish_time_condition = 2
    start_time = datetime.now()

    # Algorithm Parameters
    population_size = 10
    numberOfInputs = 5
    mutation_rate = 0.8
    numberOfGenerations = 20
    dependencyB = 0.5
    maximumNumberOfChildren = 10

    # Initialization of population
    individualConditions = IndividualConditions

    population = Population()
    population.generateInitialPopulation(population_size)
    ga.population = population

    fittestOfGenerations = []

    for generation in range(1, numberOfGenerations):
        generationArrays = GenerationArrays()
        array = ga.getPopulationArrays()
        generationArrays.extendArrays(array)
        individual = population.individuals[0]
        evaluatedIndividual = fitness(individual, numberOfInputs)
        print('[individual, y, surface, score] = ', evaluatedIndividual)

        # Adds score of individual in score curve
        score_curve.append(evaluatedIndividual[3])

        numberOfIndividualsWillCopulate = randint(1, population_size)
        print('Number of crossover: ', numberOfIndividualsWillCopulate)

        while population.size < numberOfIndividualsWillCopulate:
            numberOfIndividualsWillCopulate -= 1

        for crossoverQuantity in range(1, numberOfIndividualsWillCopulate):

            if population.size >= 3:

                # Selection
                [father, evaluatedListWithoutFather] = ga.getBest(population.individuals, numberOfInputs)
                print('father = ', father)
                populationWithoutFather = ga.getPopulationFromGeneration(evaluatedListWithoutFather)

                [mother, evaluatedListWithoutParents] = ga.getBest(populationWithoutFather, numberOfInputs)
                print('mother = ', mother)

                populationWithoutParents = ga.getPopulationFromGeneration(evaluatedListWithoutParents)

                maxChildren = randint(1, maximumNumberOfChildren)
                for numberOfChildren in range(1, maximumNumberOfChildren):
                    # Crossover
                    child = ga.crossover(father, mother)
                    evaluatedChild = fitness(child, numberOfInputs)
                    scoreChild = evaluatedChild[3]

                    # With this verification we guarantee that child is ever better than their parents
                    #while scoreChild > father[3] and scoreChild > mother[3]:
                    #    child = ga.crossover(father, mother)
                    #    evaluatedChild = fitness(child, numberOfInputs)
                    #    scoreChild = evaluatedChild[3]

                    print('child = [', child.wn, child.ln, child.wp, child.lp, ']')
                    print('[child, y, surface, score] = ', evaluatedChild)

                    # Add the child in population
                    population.individuals.append(child)

                    # Add score of child in curve
                    score_curve.append(evaluatedChild[3])

        # Mutations

        histocastic_factor_of_mutation = uniform(0, 1)
        print('\n Histocastic factor of mutation:  ', histocastic_factor_of_mutation)
        print('Mutation rate:  ', mutation_rate)

        if histocastic_factor_of_mutation <= mutation_rate:

            # Select an indiviudal of current generation to be mutated
            individualIndex = randint(1, population.individuals.__len__()-1)
            individualToBeMutated = population.individuals.__getitem__(individualIndex)
            print('\nIndividual to be mutated:\n')

            mutant = ga.mutation(individualToBeMutated, individualConditions, generation, numberOfGenerations,
                                 dependencyB)
            print("\nmutant:\n")

            evaluatedMutant = fitness(mutant, numberOfInputs)
            print('Eavaluated mutant: \n [mutant, y, surface, score] = ', evaluatedMutant)

            # Add mutant score in curve
            score_curve.append(evaluatedMutant[3])

            # Removing the status of individual before mutation and add the individual mutated on population
            population.individuals.remove(individualToBeMutated)
            population.individuals.append(mutant)

        # Kill the worst individuals

        populationWithoutWorse = []
        division_point = int(0.5 * population.size)
        numberOfDeathsInGeneration = randint(0, division_point)

        for numberOfDeaths in range(0, numberOfDeathsInGeneration):
            [worse, evaluatedListWithoutWorse] = ga.getWorse(population.individuals, numberOfInputs)
            #print('worse = ', worse)
            populationWithoutWorse = ga.getPopulationFromGeneration(evaluatedListWithoutWorse)

        # Simulating the death of individuals with population without parents and less fit individual
        population.individuals = populationWithoutWorse

        if population.size == 0:
            break

        # Fittest
        print("\n\n Fittest Finding: ")
        print("List of evaluated: ")
        [fitestOfGenerationList, evaluatedListWithoutParents] = ga.getBest(population.individuals, numberOfInputs)
        print('Fittest individual: ')
#        fitestOfGenerationList[0].print()

        # Adding fittest score of generation in curve
        best_score_curve.append(fitestOfGenerationList[3])

        # Saving the fittest
        fittestOfGenerations.append(fitestOfGenerationList[0])

        elapsedTime = datetime.now() - start_time

        if elapsedTime.min >= timedelta(minutes=finish_time_condition):
            break

    print('=================================================================')
    print('Fittest:')
    for x in fittestOfGenerations:
        evalX = fitness(x, numberOfInputs)
    print('Individual: ')
    print('['+str(evalX[0].wn)+','+str(evalX[0].wp)+','+str(evalX[0].ln)+','+str(evalX[0].lp)+']')
    print('Score:', evalX[3])
    print('----------------------------------------------------------')

    ga.plotGraph(best_score_curve, "bestScore.png", "Score", "Generation", "Evolution", numberOfGenerations)
