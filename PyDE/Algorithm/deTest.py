import numpy as np
import random as rand

mili = 10e-3
micro = 10e-6
nano = 10e-9
pico = 10e-12
fanton = 10e-15

maxWn = 0.36 * micro
maxLn = 0.24 * micro
minWn = 32 * nano
minLn = 11 * nano
maxWp = 2 * maxWn
maxLp = 2 * maxLn
minWp = 2 * minWn
minLp = 2 * minLn


class Results:
    def __init__(self):
        self.TPC
        self.TEQ
        self.SRF
        self.BInd


results = Results

individual = {
    'wn': (minWn, maxWn),
    'wp': (minWp, maxWp),
    'ln': (minLn, maxLn),
    'lp': (minLp, maxLp),
    'm': 10
}

options = {'c1': 0.5, 'c2': 0.3, 'w': 0.9}


def fitness():
    wn = rand.uniform(individual.get('wn')[0], individual.get('wn')[1])
    wp = rand.uniform(individual.get('wp')[0], individual.get('wp')[1])
    ln = rand.uniform(individual.get('ln')[0], individual.get('ln')[1])
    lp = rand.uniform(individual.get('lp')[0], individual.get('lp')[1])
    M = individual.get('m')

    # Physical Conditions
    Cox = 6 * fanton
    unCox = 115 * micro
    upCox = 30 * micro
    Vth_n = 0.4
    Vth_p = - Vth_n
    Vdd = 2.5
    Cj = 0.2 * fanton
    Cjsw = 0.5 * fanton
    # Technology
    LdiffMargin = 0.2
    Ldiff = LdiffMargin * (ln + lp)

    # Expression Load Capacitance

    C = Cox * M * (wp * lp + wn * ln) + Cj * Ldiff * (wn + wp) + Cjsw * (wn + wp + 4 * Ldiff)

    tphl = C * Vth_n / ((1 / 2) * unCox * (wn / ln) * (Vdd - Vth_n) ** 2)
    tplh = C * Vth_p / ((1 / 2) * upCox * (wp / lp) * (Vdd - Vth_p) ** 2)

    transientPerformanceCondition = abs((wn / ln) - (M / 2) * (wp / lp))
    timeEqualityPerformance = abs(tphl - tplh)

    y = abs(transientPerformanceCondition + timeEqualityPerformance)

    surface = (wn * ln + wp * lp) * (1.2)  # Adds 20% for the trails

    score = y + surface * (10 ** (12))

    results.TPC = transientPerformanceCondition
    results.TEQ = timeEqualityPerformance
    results.SRF = surface
    results.BInd = [wn, ln, wp, lp]

    return score


def f(x):
    n_particles = x.shape[0]
    j = [fitness() for i in range(n_particles)]
    return np.array(j)

#bounds =

#de = DE(minfun, bounds, npop)