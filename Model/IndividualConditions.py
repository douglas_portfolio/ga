from FitnessFunctions.units import *

class IndividualConditions:
    maxWn = 0.36*micro
    maxLn = 0.24*micro
    minWn = 32*nano
    minLn = 11*nano
    maxWp = 2*maxWn
    maxLp = 2*maxLn
    minWp = 2*minWn
    minLp = 2*minLn