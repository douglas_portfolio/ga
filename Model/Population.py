from Model import Individual, IndividualConditions
import numpy as np
import datetime

class Population:
    individuals = []
    __size__ = 0

    @property
    def size(self):
        self.__size__ = self.individuals.__sizeof__()
        return self.__size__

    def add_individual(self, individual):
        self.individuals.append(individual)
        self.__size__ += 1
        return self.individuals

    def remove_individual(self, individual):
        self.individuals.remove(individual)
        self.__size__ -= 1
        return self.individuals

    def generateInitialPopulation(self, popuation_size):

        if self.__size__ is not 0:
            raise "Population already exists"

        popSet = set([])
        individualConditions = IndividualConditions.IndividualConditions
        wnValues = np.random.uniform(individualConditions.minWn, individualConditions.maxWn,(popuation_size,))
        wpValues = np.random.uniform(individualConditions.minWp, individualConditions.maxWp,(popuation_size,))
        lnValues = np.random.uniform(individualConditions.minLn, individualConditions.maxLn,(popuation_size,))
        lpValues = np.random.uniform(individualConditions.minLp, individualConditions.maxLp,(popuation_size,))

        for i in range(0, popuation_size):
            ind = Individual.Individual()
            ind.wn = wnValues[i]
            ind.wp = wpValues[i]
            ind.ln = lnValues[i]
            ind.lp = lpValues[i]

            popSet.add(ind)

        self.individuals = list(popSet)
