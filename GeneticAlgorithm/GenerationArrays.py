class GenerationArrays:
    def __init__(self):
        self.wnArray = []
        self.wpArray = []
        self.lnArray = []
        self.lpArray = []

    def extendArrays(self, generationArray):
        self.wnArray.extend(generationArray[0])
        self.wpArray.extend(generationArray[1])
        self.lnArray.extend(generationArray[2])
        self.lpArray.extend(generationArray[3])

    def getAllGenerationArrays(self):
        return [self.wnArray, self.wpArray, self.lnArray, self.lpArray]