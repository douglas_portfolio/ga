def normal(self, value, deviation):
    result = (-0.5) * (value / deviation) ** 2
    result = math.exp(result)
    result = result / (deviation * math.sqrt(2 * math.pi))
    return result


def integral(lim_sup, lim_inf, deltaX):
    result = 0
    sum = 0
    aux = normal(lim_inf, 1)
    infDelta = deltaX + lim_inf
    for i in range(infDelta, lim_sup):
        sum = normal(i, 1)
        result = result + aux + sum
        aux = sum
        deltaX = deltaX + i
    result = result * delta / 2
    return result


def disturbance(probability=0.5, deltaX=0.1, value=0):
    result = value

    if value == 0:
        result = -5;

    sum = 0;
    while (sum < prob):
        result = result + deltaX
        sum = sum + integral(result, result - deltaX)

    return result
