import math
import random

import matplotlib.pyplot as plt

from FitnessFunctions.fitness_methods import fitness
from GeneticAlgorithm.math_utils import *
from Model.Individual import Individual
from Model.Population import Population


# I'M SEPARATING THE METHODS IN CLASSES TO IMPLEMENT WITH OBJECT ORIENTED
# BUT I HAVE SAVE THE ORIGINAL IN THE MASTER BRANCH.
# YOU CAN SEE THAT WE ARE ON THE objectOrientedRefactor BRANCH.
# YOU HAVE TO FIX THOSE ISSUES CALLING THE RIGHT METHODS FROM THE OBJECTS.


class GeneticAlgorithm:

    population = Population()

    def __init__(self):
        pass

    def evaluateAndSwapIndividual(self, individual, individualConditions):
        while individual.wn < individualConditions.minWn or individual.wn > individualConditions.maxWn:
            individual.wn = random.uniform(individualConditions.minWn, individualConditions.maxWn)

        while individual.ln < individualConditions.minLn or individual.ln > individualConditions.maxLn:
            individual.ln = random.uniform(individualConditions.minLn, individualConditions.maxLn)

        while individual.wp < individualConditions.minWp or individual.wp > individualConditions.maxWp:
            individual.wp = random.uniform(individualConditions.minWp, individualConditions.maxWp)

        while individual.lp < individualConditions.minLp or individual.lp > individualConditions.maxLp:
            individual.lp = random.uniform(individualConditions.minLp, individualConditions.maxLp)

        return individual


    def plotGraph(self, longArray, filePath, yAxisName, xAxisName, graphTitle, xLimit):
        x = list(range(0, xLimit))
        fig, ax = plt.subplots(nrows=1, ncols=1)  # create figure & 1 axis
        ax.plot(longArray)
        plt.xlabel(xAxisName)
        plt.ylabel(yAxisName)
        plt.title(graphTitle)

        plt.show()

    ###
    # Returns a list with 4 arrays in population: wn, wp, ln, lp
    ###
    def getPopulationArrays(self):
        wnArray = [i.wn for i in self.population.individuals]
        wpArray = [i.wp for i in self.population.individuals]
        lnArray = [i.ln for i in self.population.individuals]
        lpArray = [i.lp for i in self.population.individuals]

        return [wnArray, wpArray, lnArray, lpArray]


    def populationFitness(self, evaluatedIndividualsList):
        bestScore = 1000 # a very great number to be changed by loop

        for evalInd in evaluatedIndividualsList:
            indSocore = evalInd[3]
            popScore = disturbance(value = indSocore)
            if(popScore < bestScore):
                bestScore = popScore

        return bestScore

    def getWorse(self, evaluationPop, numberOfInputs):
        evaluatedList = []
        for individual in evaluationPop:
            evaluatedIndividual = fitness(individual, numberOfInputs)
            evaluatedList.append(evaluatedIndividual)

        maximum = max(x[3] for x in evaluatedList)
        index = 0
        worseIndex = 0
        worseEvaluated = []
        for evInd in evaluatedList:
            if evInd[3] == maximum:
                worseEvaluated = evInd
            index = index + 1

        listWithoutWorse =  evaluatedList
        del listWithoutWorse[worseIndex]

        return [worseEvaluated, listWithoutWorse]

    def getBest(self, evaluationPop, numberOfInputs):
        if(evaluationPop == []):
            raise Exception('Stoping here. Extintion!')

        evaluatedList = []
        for individual in evaluationPop:
            evaluatedIndividual = fitness(individual,numberOfInputs)
            evaluatedList.append(evaluatedIndividual)

        minimum = min(x[3] for x in evaluatedList)
        index = 0
        fitestIndex = 0
        fitestEvaluated = None
        for evInd in evaluatedList:
            print(index, ' - ', evInd)
            if evInd[3] == minimum:
                fitestEvaluated = evInd
                fitestIndex = index
                break
            else:
                index = index + 1

        listWithoutFitest = evaluatedList
        del listWithoutFitest[fitestIndex]

        return [fitestEvaluated, listWithoutFitest]


    def getPopulationFromGeneration(self, evaluatedList):
        populationOfGeneration = []

        for x in evaluatedList:
            populationOfGeneration.append(x[0])

        return populationOfGeneration

    def __arithmeticalXOverPointCalculation__(self, floatNumberFather, floatNumberMother, alpha, supLim, infLim):
        if alpha < 0 or alpha > 1:
            raise OverflowError('The alpha factor is out of bound.')

        return alpha * floatNumberFather + (1 - alpha) * floatNumberMother + random.uniform(supLim, infLim)

    def crossover(self, father, mother):
        fatherWn = father[0].wn
        fatherLn = father[0].ln
        fatherWp = father[0].wp
        fatherLp = father[0].lp

        motherWn = mother[0].wn
        motherLn = mother[0].ln
        motherWp = mother[0].wp
        motherLp = mother[0].lp

        import Model.IndividualConditions as IndividualConditions
        cond = IndividualConditions.IndividualConditions
        alpha = random.uniform(0,1)
        childWn = self.__arithmeticalXOverPointCalculation__(fatherWn, motherWn, alpha, cond.maxWn, cond.minWp)
        childLn = self.__arithmeticalXOverPointCalculation__(fatherLn, motherLn, alpha, cond.maxLn, cond.minLn)
        childLp = self.__arithmeticalXOverPointCalculation__(fatherLp, motherLp, alpha, cond.maxLp, cond.minLp)
        childWp = self.__arithmeticalXOverPointCalculation__(fatherWp, motherWp, alpha, cond.maxWp, cond.minWp)

        child = Individual()
        child.wn = childWn
        child.wp = childWp
        child.ln = childLn
        child.lp = childLp

        return child

    # Mutation methods
    ###############################################
    #@t: index of current generation
    #@y: the maximum value of mutation
    #@gMax: the number maximun of generations that we will execute
    #@r: a number between 0 and 1
    #@b: it is a parameter that controls the grade of dependency of value
    # of mutation with the number of generations. Which grater is this value,
    # faster the calculated value for delta
    ###############################################
    def __deltaTY__(self, t,y,r,gMax,b):
        exponent = (1 - gMax)**b
        root = r**exponent
        delta = y * (1 - root)
        return delta

    def ununiformMutationTau(self, tau, mutationGene,t,y,r,gMax,b):
        factor = 0
        if tau == 1:
            factor = - self.__deltaTY__(t,y,r,gMax,b)
        elif tau == 0:
            factor = self.__deltaTY__(t,y,r,gMax,b)

        mutant = mutationGene + factor

        if isinstance(mutant, complex):
            mutant = (mutant.real ** 2 + mutant.imag ** 2) ** 0.5

        return mutant

    def __getGene__(self, x, individual):
        return {
            1: individual.wn,
            2: individual.wp,
            3: individual.ln,
            4: individual.lp
        }[x]

    def getMinimumValueRelatedToGene(self, x, individualCondition):
        return {
            1: individualCondition.minWn,
            2: individualCondition.minWp,
            3: individualCondition.minLn,
            4: individualCondition.minWp
        }[x]

    def getMaximumValueRelatedToGene(self, x, individualCondition):
        return {
            1: individualCondition.maxWn,
            2: individualCondition.maxWp,
            3: individualCondition.maxLn,
            4: individualCondition.maxLp
        }[x]

    def setIndividualGene(self, index, individual, geneValue):
        if index == 1:
            individual.wn = geneValue
        elif index == 2:
            individual.wp = geneValue
        elif index == 3:
            individual.ln = geneValue
        elif index == 4:
            individual.lp = geneValue

        return individual

    #p. 267
    def mutation(self, individual, individualCondition, indexCurrentGeneration, maxGenerations, dependencyB):
        tau = random.getrandbits(1)
        characteristicMutationIndex = random.randint(1,4)

        r = random.uniform(0, 1)

        mutationGene = self.__getGene__(characteristicMutationIndex, individual)
        maximumValueOfGene = self.getMaximumValueRelatedToGene(characteristicMutationIndex, individualCondition)
        geneMutated = self.ununiformMutationTau(tau, mutationGene,indexCurrentGeneration,maximumValueOfGene,r,maxGenerations,
                                           dependencyB)

        individual = self.setIndividualGene(characteristicMutationIndex, individual, geneMutated)

        return individual


    def plotCurves(curvesMap, infLim0x, step0x, supLimOx):
        ox = math.linspace(infLim0x, step0x, supLimOx)
        if len(curvesMap) == 0 :
            print('There are no curves.')
            return

        for curveName in curvesMap:
            curve = curvesMap[curveName]
            plt.plot(ox, curve, curveName)
